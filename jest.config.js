module.exports = {
  projects: [
    '<rootDir>/apps/react-app',
    '<rootDir>/apps/angular-app',
    '<rootDir>/apps/myapp',
    '<rootDir>/apps/node-app',
    '<rootDir>/apps/express-app',
  ],
};
